<?php

namespace GlideReviews\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ReviewServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        //dependencies
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $config = $serviceLocator->get('Config');
        $authUserService = $serviceLocator->get($config['GlideReviews']['UserAuthenticationService']);

        //dependency injections
        return new ReviewService($entityManager, $config, $authUserService);
    }

}
