<?php

namespace GlideReviews\Service;

use Doctrine\ORM\EntityManager;
use GlideReviews\Entity\Review;
use GlideReviews\Entity\ReviewItemInterface;
use GlideReviews\Entity\UserInterface;

class ReviewService {

    private $entityManager;
    private $authentication;
    private $config;

    public function __construct(EntityManager $entityManager, $config, $authentication) {
        $this->entityManager = $entityManager;
        $this->authentication = $authentication;
        $this->config = $config;
    }

    /**
     * Creates a review entry for the review $item
     *
     * @param ReviewItemInterface $item
     * @param UserInterface $customer
     * @param UserInterface $producer
     */
    public function createReview(ReviewItemInterface $item, $customer, $producer) {
        $reviewItem = $this->entityManager->getRepository('GlideReviews\Entity\Review')->findOneBy(array('reviewItem' => $item));

        if (!empty($reviewItem))
            throw new \Exception('Review item exists');

        $review = new Review();
        $review->setReviewItem($item);
        $review->setCustomer($customer);
        $review->setProducer($producer);
        $this->entityManager->persist($review);
    }

    /**
     * Set review $item producer field values
     *
     * @param ReviewItemInterface $item
     * @param array $values Associative array of review field values
     */
    public function setProducerFields(ReviewItemInterface $item, $values) {
        $producerMappings = $this->config['GlideReviews']['ProducerMappings'];

        $review = $this->entityManager->getRepository('GlideReviews\Entity\Review')->findOneBy(array('reviewItem' => $item));
        if (empty($review) || $review->getProducer() != $this->authentication->getIdentity())
            throw new \Exception('Not producer');

        switch ($review->getStatus()) {
            case Review::STATUS_BOTH_COMPLETED:
            case Review::STATUS_PRODUCER_COMPLETED:
                throw new \Exception('Review item already completed');
                break;
        }

        $this->setFields($review, $values, $producerMappings);

        $complete = true;
        foreach ($producerMappings as $key => $val)
            if (empty($review->{"get" . ucfirst($val)}()))
                $complete = false;
        if (!$complete)
            throw new \Exception('Fields incomplete');

        $review->setProducerCompletionDate(new \DateTime());
        if ($review->getStatus() == Review::STATUS_CREATED)
            $review->setStatus(Review::STATUS_PRODUCER_COMPLETED);
        else {
            $review->setBothCompleteDate(new \DateTime());
            $review->setStatus(Review::STATUS_BOTH_COMPLETED);
        }

        $this->entityManager->persist($review);
    }

    /**
     * Set review $item customer field values
     *
     * @param ReviewItemInterface $item
     * @param array $values Associative array of review field values
     */
    public function setCustomerFields(ReviewItemInterface $item, $values) {
        $customerMappings = $this->config['GlideReviews']['CustomerMappings'];

        $review = $this->entityManager->getRepository('GlideReviews\Entity\Review')->findOneBy(array('reviewItem' => $item));
        if (empty($review) || $review->getCustomer() != $this->authentication->getIdentity())
            throw new \Exception('Not customer');

        switch ($review->getStatus()) {
            case Review::STATUS_BOTH_COMPLETED:
            case Review::STATUS_CUSTOMER_COMPLETED:
                throw new \Exception('Review item already completed');
                break;
        }


        $this->setFields($review, $values, $customerMappings);

        $complete = true;
        foreach ($customerMappings as $key => $val)
            if (empty($review->{"get" . ucfirst($val)}()))
                $complete = false;
        if (!$complete)
            throw new \Exception('Fields incomplete');

        $review->setCustomerCompletionDate(new \DateTime());
        if ($review->getStatus() == Review::STATUS_CREATED)
            $review->setStatus(Review::STATUS_CUSTOMER_COMPLETED);
        else {
            $review->setBothCompleteDate(new \DateTime());
            $review->setStatus(Review::STATUS_BOTH_COMPLETED);
        }

        $this->entityManager->persist($review);
    }

    /**
     * Set $review field values
     *
     * @param Review $review
     * @param array $values Associative array of review field values
     * @param array $mapping Associative array of field mappings
     */
    private function setFields(Review $review, $values, $mappings) {

        $expiry = new \DateTime();
        $expiresAfterDays = $this->config['GlideReviews']['ExpiresAfter'];
        $expiry->modify('-' . $expiresAfterDays . ' days');

        if ($review->getGeneratedTimestamp() < $expiry)
            throw new \Exception('Review expired');


        foreach ($values as $key => $val) {
            if (!isset($mappings[$key]))
                throw new \Exception('Field mapping not found:' . $key);

            $field = $mappings[$key];

            if (strpos($field, 'Stars') !== false) {
                if (!(is_int($val) && $val > 0 && $val < 6))
                    throw new \Exception('Invalid star value');
            }
            else if (strpos($field, 'YesNo') !== false) {
                if (!(is_int($val) && ($val == -1 || $val == 1)))
                    throw new \Exception('Invalid yes/no value');
            }
            else if (strpos($field, 'Text') !== false) {
                $val = trim($val);
                $filter = new \Zend\Filter\StripTags();
                $val = $filter->filter($val);
                if (!(is_string($val) && strlen($val) >= 15))
                    throw new \Exception('Invalid text value');
            }

            $review->{"set" . ucfirst($field)}($val);
        }
    }

    /**
     * Returns any outstanding reviews for the currently authenticated user.
     * 
     * @param bool $ids Return ids or objects
     */
    public function getOutstanding($ids = false) {
        $user = $this->authentication->getIdentity();

        $query = $this->entityManager->createQuery("
            SELECT r
            FROM GlideReviews\Entity\Review r
            WHERE (r.customer = :user OR r.producer = :user)
            AND r.status <> :status
            AND r.generatedTimestamp > :expiry
            ORDER BY r.status DESC, r.generatedTimestamp");
        $query->setParameter("status", Review::STATUS_BOTH_COMPLETED);
        $query->setParameter("user", $user);

        $expiry = new \DateTime();
        $expiresAfterDays = $this->config['GlideReviews']['ExpiresAfter'];
        $expiry->modify('-' . $expiresAfterDays . ' days');
        $query->setParameter("expiry", $expiry);
        $result = $query->getResult();

        $reviews = array();

        foreach ($result as $review) {
            if ($review->getCustomer() == $user) {
                if ($review->getStatus() == Review::STATUS_CUSTOMER_COMPLETED)
                    continue;
            }
            else {
                if ($review->getStatus() == Review::STATUS_PRODUCER_COMPLETED)
                    continue;
            }

            if ($ids)
                $reviews[] = $review->getId();
            else
                $reviews[] = $review;
        }

        return $reviews;
    }

    /**
     * Returns public reviews for $user as customer
     * 
     * @param GlideReviews\Entity\UserInterface $user
     * @return array Customer review objects for $user
     */
    public function getCustomerReviewsReceived($user) {

        $query = $this->entityManager->createQuery("
            SELECT r
            FROM GlideReviews\Entity\Review r
            WHERE r.customer = :user
            AND r.status = :status
            ORDER BY r.generatedTimestamp DESC");
        $query->setParameter("status", Review::STATUS_BOTH_COMPLETED);
        $query->setParameter("user", $user);
        $reviews = $query->getResult();

        return $reviews;
    }

    /**
     * Returns public reviews for $user as producer
     * 
     * @param GlideReviews\Entity\UserInterface $user
     * @return array Producer review objects for $user
     */
    public function getProducerReviewsReceived($user) {

        $query = $this->entityManager->createQuery("
            SELECT r
            FROM GlideReviews\Entity\Review r
            WHERE r.producer = :user
            AND r.status = :status
            ORDER BY r.generatedTimestamp DESC");
        $query->setParameter("status", Review::STATUS_BOTH_COMPLETED);
        $query->setParameter("user", $user);
        $reviews = $query->getResult();

        return $reviews;
    }

    /**
     * Returns review details
     * Will return only the appropriate fields depending on if user is a customer or producer
     * 
     * @param mixed $review Review object or id
     * @param GlideReviews\Entity\UserInterface $user
     * @param array $fields Array of fields to be returned. It empty then all fields returned for either customer or producer
     * 
     */
    public function getReviewDetails($review, $user, $fields = array()) {
        $id = is_int($review) ? $review : $review->getId();

        $query = $this->entityManager->createQuery("
            SELECT r
            FROM GlideReviews\Entity\Review r
            WHERE (r.customer = :user OR r.producer = :user)
            AND r.id = :id");
        $query->setParameter("id", $id);
        $query->setParameter("user", $user);
        $reviews = $query->getResult();

        foreach ($reviews as $review) {
            $return = array('reviewItem' => $review->getReviewItem()->getId());

            if ($review->getCustomer() == $user) {
                // We return the fields with review data from other user 
                $mappings = $this->config['GlideReviews']['ProducerMappings'];
                $return['type'] = 'customer';
            } else if ($review->getProducer() == $user) {
                // We return the fields with review data from other user 
                $mappings = $this->config['GlideReviews']['CustomerMappings'];
                $return['type'] = 'producer';
            } else
                throw new \Exception();

            foreach ($mappings as $key => $val) {
                if (empty($fields) || in_array($key, $fields))
                    $return[$key] = $review->{"get" . ucfirst($val)}();
            }

            return $return;
            break;
        }
    }

    /**
     * Returns review object
     * 
     * @param int $id Review id
     * @return GlideReviews\Entity\Review
     */
    public function get_review($id) {
        return $this->entityManager->getRepository('GlideReviews\Entity\Review')->find($id);
    }

    /**
     * Returns review object
     *
     * @param ReviewItemInterface $item
     * @return GlideReviews\Entity\Review
     */
    public function get_review_by_item(ReviewItemInterface $item) {
        return $this->entityManager->getRepository('GlideReviews\Entity\Review')->findOneBy(array('reviewItem'=>$item));
    }

}
