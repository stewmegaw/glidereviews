<?php

namespace GlideReviews\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="review")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Review {

    const STATUS_CREATED = 1;
    const STATUS_PRODUCER_COMPLETED = 2;
    const STATUS_CUSTOMER_COMPLETED = 3;
    const STATUS_BOTH_COMPLETED = 4;

    /**
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlideReviews\Entity\ReviewItemInterface")
     */
    protected $reviewItem;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlideReviews\Entity\UserInterface")
     */
    protected $customer;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlideReviews\Entity\UserInterface")
     */
    protected $producer;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $producerStars1;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $producerStars2;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $producerStars3;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $producerStars4;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $producerStars5;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $producerYesNo1;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $producerText1;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $customerStars1;

    /**
     *
     * @ORM\Column(type="smallint", length=1, options={"default"=0}, nullable=true)
     *
     */
    protected $customerYesNo1;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $customerText1;

    /**
     *
     * @ORM\Column(type="datetime")
     *
     */
    protected $generatedTimestamp;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $customerCompletionDate;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $producerCompletionDate;
    
    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $bothCompleteDate;

    /**
     *
     * @ORM\Column(type="smallint", options={"default"=1}, nullable=true)
     *
     */
    protected $status = 1;


    public function __construct() {
        $this->generatedTimestamp = new \DateTime();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getReviewItem() {
        return $this->reviewItem;
    }

    public function setReviewItem($reviewItem) {
        $this->reviewItem = $reviewItem;
        return $this;
    }

    public function getCustomer() {
        return $this->customer;
    }

    public function setCustomer($customer) {
        $this->customer = $customer;
        return $this;
    }

    public function getProducer() {
        return $this->producer;
    }

    public function setProducer($producer) {
        $this->producer = $producer;
        return $this;
    }

    public function getGeneratedTimestamp() {
        return $this->generatedTimestamp;
    }

    public function setGeneratedTimestamp($generatedTimestamp) {
        $this->generatedTimestamp = $generatedTimestamp;
        return $this;
    }
    
    public function getCustomerCompletionDate() {
        return $this->customerCompletionDate;
    }

    public function setCustomerCompletionDate($CustomerCompletionDate) {
        $this->customerCompletionDate = $CustomerCompletionDate;
        return $this;
    }
    
    public function getProducerCompletionDate() {
        return $this->producerCompletionDate;
    }

    public function setProducerCompletionDate($ProducerCompletionDate) {
        $this->producerCompletionDate = $ProducerCompletionDate;
        return $this;
    }
    
    public function getBothCompleteDate() {
        return $this->bothCompleteDate;
    }

    public function setBothCompleteDate($BothCompleteDate) {
        $this->bothCompleteDate = $BothCompleteDate;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }
    
    public function getProducerStars1() {
        return $this->producerStars1;
    }

    public function setProducerStars1($producerStars1) {
        $this->producerStars1 = $producerStars1;
        return $this;
    }

    public function getProducerStars2() {
        return $this->producerStars2;
    }

    public function setProducerStars2($producerStars2) {
        $this->producerStars2 = $producerStars2;
        return $this;
    }

    public function getProducerStars3() {
        return $this->producerStars3;
    }

    public function setProducerStars3($producerStars3) {
        $this->producerStars3 = $producerStars3;
        return $this;
    }

    public function getProducerStars4() {
        return $this->producerStars4;
    }

    public function setProducerStars4($producerStars4) {
        $this->producerStars4 = $producerStars4;
        return $this;
    }

    public function getProducerStars5() {
        return $this->producerStars5;
    }

    public function setProducerStars5($producerStars5) {
        $this->producerStars5 = $producerStars5;
        return $this;
    }

    public function getProducerYesNo1() {
        return $this->producerYesNo1;
    }

    public function setProducerYesNo1($producerYesNo1) {
        $this->producerYesNo1 = $producerYesNo1;
        return $this;
    }

    public function getProducerText1() {
        return $this->producerText1;
    }

    public function setProducerText1($producerText1) {
        $this->producerText1 = $producerText1;
        return $this;
    }

    public function getCustomerStars1() {
        return $this->customerStars1;
    }

    public function setCustomerStars1($customerStars1) {
        $this->customerStars1 = $customerStars1;
        return $this;
    }

    public function getCustomerYesNo1() {
        return $this->customerYesNo1;
    }

    public function setCustomerYesNo1($customerYesNo1) {
        $this->customerYesNo1 = $customerYesNo1;
        return $this;
    }

    public function getCustomerText1() {
        return $this->customerText1;
    }

    public function setCustomerText1($customerText1) {
        $this->customerText1 = $customerText1;
        return $this;
    }

}
