- composer require stewmegaw/glidereviews dev-master
- Copy /config/glidereviews.global.php.dist to /config/autoload and remove .dist extension
 - Customize config
- Add 'GlideReviews' to application.config.php module list
- Review item entity must implement PaymentItemInterface
- User item entity must implement UserInterface
- Update doctrine


- Example Usage


1) Bid was accepted and project+payments are complete. A cron to create review might look like:

******
$datetime = new \DateTime();
$datetime->modify('+' . self::ReviewCreateDelay . ' hours');

// Get all accepted offers in the past with no review
// Using sql as dql does not support Right Join
$sql = "
SELECT o.id,o.user_from_id,t.user_id
FROM review AS r
RIGHT JOIN offer AS o ON o.id = r.reviewItem_id
JOIN trip AS t ON o.trip_id = t.id
WHERE o.status = " . Offer::OFFER_STATUS_ACCEPTED . "
AND o.until_date <= '" . $datetime->format('Y-m-d h:i:s') . "'
AND r.id IS NULL";

$offerRepo = $this->entityManager->getRepository('Trip\Entity\Offer');

$direct_db_connection = $this->entityManager->getConnection();
$iterator = $direct_db_connection->query($sql);
while (is_object($iterator) AND ( $r = $iterator->fetch()) !== FALSE) {
    $this->reviewService->createReview($offerRepo->find($r['id']), $this->userService->get_user($r['user_id']), $this->userService->get_user($r['user_from_id']));
}

$this->entityManager->flush();
*********


2) When page loads check for outstanding reviews:

*********
$review_ids = $this->reviewService->getOutstanding(true);
*********


3) Using a review id get review details/questions for UI:

*********
$info = array(
    'review' => $this->reviewService->getReviewDetails((int)$id, $this->userService->get_user()),
    'details' => $this->userService->getDetailsOfReview($id, $this->userService->get_user()),
)
*********


4) Save review info entered by user

*********
$review = $this->reviewService->get_review($id);
if ($review->getProducer() == $this->userService->get_user())
    $this->reviewService->setProducerFields($review->getReviewItem(), $data);
else
    $this->reviewService->setCustomerFields($review->getReviewItem(), $data);

$this->entityManager->flush();
*********


5) List all producer/customer reviews for a user

*********
$user = $this->userService->get_user($id);
$reviews = array('customer' => array(), 'producer' => array());
$customerReviews = $this->reviewService->getCustomerReviewsReceived($user);
foreach ($customerReviews as $idx => $review)
    $reviews['customer'][$idx] = array_merge($this->userService->getDetailsOfReview($review->getId(), $user), $this->reviewService->getReviewDetails($review, $user, null));
$producerReviews = $this->reviewService->getProducerReviewsReceived($user);
foreach ($producerReviews as $idx => $review)
    $reviews['producer'][$idx] = array_merge($this->userService->getDetailsOfReview($review->getId(), $user), $this->reviewService->getReviewDetails($review, $user, null));
return array('reviews' => $reviews);
*********


6) Example jsx for building review stars. Amend for plain javascript

*********
var star = {
	get_stars: function(num){
		var stars = [];

		for(var i = 1; i <= num; i++)
			stars.push(<StarIconFull/>);
		if((num/0.5)%2!=0)
			stars.push(<StarIconHalf/>);
		for(var i = Math.ceil(num)+1; i <= 5; i++)
 				stars.push(<StarIconEmpty/>);

		return stars;
	},
	get_average_stars: function(d){
		if(!d.length)
		{
			var stars = [];
			for(var i = 1; i <= 5; i++)
			{
				stars.push(
					<StarIconEmpty/>
  	 			);
			}
			return stars;
		}

		var total = 0;
		for(var i = 0; i< d.length; i++)
		{
			total += d[i].overall;
		}
		
		return star.get_stars(Math.round((total/d.length)*2)/2);
	}
}

// Get average review for customer
console.log(star.get_average_stars(r.customerReviews));
// Get stars for an individual review
console.log(star.get_stars(r.review.overall));
*********